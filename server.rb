require "rubygems"
require "bundler/setup"

# require 'rack/cors'
require "goliath"

require_relative "api.rb"

class Server < Goliath::API
  use Rack::Static, root: File.join(File.dirname(__FILE__), "public"), urls: ["/scripts","/styles","/views"]

  def response(env)
    case env['PATH_INFO']
      when "/"
        [200, {"Content-Type" => "text/html"}, File.read("#{File.dirname(__FILE__)}/public/index.html")]
      else
        ::API.call(env)
    end
  end
end
