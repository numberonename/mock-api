require "grape"
require "faker"
require "json"

class GrapeError < StandardError
  attr_reader :status

  def initialize status
    @status = status
  end
end

def grape_error! message, status
  raise GrapeError.new(status), message
end

def e404
  grape_error!("Song not found", 404)
end

def get_unique_key dict
  begin
    id = Random.rand(1000).to_s
  end while dict[id]
  id
end

class API < Grape::API
  rescue_from :all do |e|
    begin
      status, message = e.status, e.message
    rescue NoMethodError
      status, message = 500, e.to_s
    end

    Rack::Response.new( {
                            error: true,
                            message: message,
                            status: status
                        }.to_json, status, { "Content-type" => "application/json" }).finish
  end

  prefix "api"
  default_format :json

  helpers do
    def permitted_params
      @permitted_params = declared(params, include_missing: false)
    end
  end

  resources = {}
  5.times do
    resources[get_unique_key(resources)] = { title: "#{Faker::Hacker.adjective.capitalize} #{Faker::Hacker.noun.capitalize}",
                                             artist: Faker::Name.name,
                                             album: Faker::Hacker.noun.capitalize,
                                             year: rand(1950..2000)
    }
  end

  #have to use class variable for mock-up. Normally you would have DB records
  get :unauthorized do
    grape_error!("You are not authorized to perform this action", 401)
  end

  resource :songs do

    desc "Returns song by id"
    get "/:id" do
      if resources[params[:id]]
        {song: resources[params[:id]]}
      else
        e404
      end
    end

    desc "Returns all songs"
    get "/" do
      {songs: resources.map{|i, res| {id: i}.merge(res) } }
    end

    desc "Create new song"
    params do
      requires :title, type: String
      requires :artist, type: String
      optional :album, type: String
      optional :year, type: Integer
    end
    post do
      resources[get_unique_key(resources)] = permitted_params
      {status: 201}
    end

    desc "Update song"
    params do
      optional :title, type: String
      optional :artist, type: String
      optional :album, type: String
      optional :year, type: Integer
    end
    put "/:id" do
      if resources[params[:id]]
        resources[params[:id]].merge! permitted_params
        {status: 200}
      else
        e404
      end
    end

    desc "Delete song"
    delete "/:id" do
      if resources[params[:id]]
        _ = resources.delete(params[:id])
        { status: 200 }
      else
        e404
      end
    end

  end

  route :any, '*path' do
    error!({ error: true, message: "Page not found", status: 404 }, 404)
  end
end

